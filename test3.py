from tkinter import *
import calendar

root = Tk()



#If you have a large number of widgets, like it looks like you will for your
#game you can specify the attributes for all widgets simply like this.
root.option_add("*Button.Background", "black")
root.option_add("*Button.Foreground", "red")

root.title('Work hours counter')
#You can set the geometry attribute to change the root windows size
#root.geometry("100x100") #You want the size of the app to be 500x500
#root.resizable(0, 0) #Don't allow resizing in the x or y direction







def foo_b1():
    l1.config(text=str(e1.get()))

def foo_b2(text):
    l1.config(text=str(text) +", "+ str(e1.get()))



e1 = Entry(root)
e1.insert(END, "anything")
e1.pack()



b1 = Button(root, text="b1", command=foo_b1)
b1.pack(padx=0, pady=0, side=LEFT)
b2 = Button(root, text="b2", command=lambda: foo_b2("foo_b2 works with an arg."))
b2.pack(padx=0, pady=0, side=RIGHT)
l1= Label(root, text="hello world")
l1.pack()


root.mainloop()
