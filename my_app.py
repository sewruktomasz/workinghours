from tkcalendar import Calendar, DateEntry
import datetime
import time
from json_operation import *

from datetime import timedelta

try:
    import tkinter as tk
    from tkinter import ttk
except ImportError:
    import Tkinter as tk
    import ttk

root = tk.Tk()


# root.geometry("500x500")


def print_sel():
    selected_day = calendar.selection_get()
    return selected_day


def update_date_show():
    label1.config(text=calendar.selection_get())


def hours_vs_time_update():
    if (work_time_vs_work_hours.get() == "work_time"):
        hours_start_entry.config(state="disabled")
        hours_end_entry.config(state="disabled")
        time_entry.config(state="normal")
        break_entry.config(state="disabled")
    else:
        hours_start_entry.config(state="normal")
        hours_end_entry.config(state="normal")
        time_entry.config(state="readonly")
        break_entry.config(state="normal")


def count_work_hours():
    try:
        if (work_time_vs_work_hours.get() == "work_hours"):
            datetimeFormat = '%H:%M:%S'
            start = str(hours_start_entry.get())
            end = str(hours_end_entry.get())
            diff = datetime.datetime.strptime(end, datetimeFormat) \
                   - datetime.datetime.strptime(start, datetimeFormat)

            # time_entry.delete()
            time_entry.config(state="normal")
            time_entry.delete(0, 'end')
            time_entry.insert(tk.END, str(diff.seconds / 60 / 60) + "h")
            time_entry.config(state="readonly")
    except NameError:
        pass


def str_date_to_date(date, sep="-"):
    result = date.split(sep)
    Y = int(result[0])
    m = int(result[1])
    d = int(result[2])
    return datetime.date(Y, m, d)


def update_month_show():
    label_choosen_month.config(text=calendar._month_names[calendar.get_displayed_month()[0]])


label_frame_calendar = tk.LabelFrame(root, text="Wybierz datę", padx=5, pady=5)
label_frame_calendar.pack(padx=10, pady=10)

calendar = Calendar(label_frame_calendar, font="Arial 14", selectmode="day", selectbackground="black", locale="pl_PL",
                    tooltipbackground="red", tooltipalpha="0.8", tooltipdelay="150")
calendar.pack(fill="both", expand=True)
calendar.bind("<<CalendarSelected>>", lambda _: update_date_show())
calendar.calevent_create(datetime.date(2019, 8, 8), text="aaa", tags=["non frei"])
calendar.calevent_create(datetime.datetime.strptime("2019-08-16", "%Y-%m-%d"), text="bbb", tags=["non frei"])

label_frame_options = tk.LabelFrame(root, text="Opcje", padx=5, pady=5)
label_frame_options.pack(padx=10, pady=10)

label1 = ttk.Label(label_frame_options, text=calendar.selection_get())
label1.grid(column=0, row=0, columnspan=2)

work_time_vs_work_hours = tk.StringVar()
work_time_vs_work_hours.set('work_hours')

work_hours_radio = tk.Radiobutton(label_frame_options, text="Godziny pracy", variable=work_time_vs_work_hours,
                                  value="work_hours", command=lambda: hours_vs_time_update())
work_hours_radio.grid(row=1, column=0)

work_time_radio = tk.Radiobutton(label_frame_options, text="Czas pracy", variable=work_time_vs_work_hours,
                                 value="work_time", command=hours_vs_time_update)
work_time_radio.grid(row=1, column=1)

hours_start_entry = tk.Entry(label_frame_options)
hours_start_entry.grid(row=2, column=0)
hours_start_entry.insert(tk.END, "6:00:00")

hours_end_entry = tk.Entry(label_frame_options)
hours_end_entry.grid(row=3, column=0)
hours_end_entry.insert(tk.END, "14:30:00")

time_entry = tk.Entry(label_frame_options)
time_entry.grid(row=2, column=1, rowspan=3)

# label_break=tk.Label(label_frame_options, text="")
break_entry = tk.Entry(label_frame_options)
break_entry.grid(row=4, column=0)
break_entry.insert(tk.END, 0.5)

tk.Label(label_frame_options, text="-----------------------------------------").grid(row=5, column=0, columnspan=2)

tk.Label(label_frame_options, text="Stawka (np. 9.79)").grid(row=6, column=0, sticky=tk.E)
rate_entry = tk.Entry(label_frame_options, )
rate_entry.grid(row=6, column=1)
rate_entry.insert(tk.END, 10.0)

work_place_label = tk.Label(label_frame_options, text="Miejsce pracy")
work_place_label.grid(row=7, column=0, sticky=tk.E)

list_of_work_places = ["Schulte", "Timbertex", "Hana", "Example"]

choosen_work_place = tk.StringVar()
choosen_work_place.set(list_of_work_places[1])

#work_place_option_menu = apply(tk.OptionMenu, (label_frame_options, choosen_work_place) + tuple(list_of_work_places))
work_place_option_menu = tk.OptionMenu(label_frame_options, choosen_work_place, *list_of_work_places, command=lambda _:print(choosen_work_place.get()))
work_place_option_menu.grid(row=7, column=1, sticky=tk.W)

everyday_costs_label=tk.Label(label_frame_options, text="Codzienne oddtrącenie")
everyday_costs_label.grid(row=8, column=0)
everyday_costs_entry=tk.Entry(label_frame_options, )
everyday_costs_entry.grid(row=8, column=1)
everyday_costs_entry.insert(tk.END, 5.50)

ok_button = tk.Button(root, text="OK", command=lambda: count_work_hours())
ok_button.pack()

label_frame_summary = tk.LabelFrame(root, text="Podsumowanie", padx=5, pady=5)
label_frame_summary.pack()

tk.Label(label_frame_summary, text="Wybany miesiąc: ").pack()
label_choosen_month = tk.Label(label_frame_summary, text="")
label_choosen_month.pack()
calendar.bind("<<CalendarMonthChanged>>", lambda _: update_month_show())

# frame_calendar = ttk.Frame(root, width=768, height=576).pack()
count_work_hours()
hours_vs_time_update()
update_month_show()
root.mainloop()
